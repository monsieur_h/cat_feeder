#!/bin/bash
echo $1
if [ "$1" == "distant" ]; then
    HOST="raspberrypi-distant"
else
    HOST="raspberrypi"
fi

REMOTE_CMD="ssh ${HOST}"
REMOTE_COPY_DIR="/tmp/catfeeder/"


${REMOTE_CMD} rm -Rf ${REMOTE_COPY_DIR}
${REMOTE_CMD} mkdir ${REMOTE_COPY_DIR}

scp -r ./* ${HOST}:${REMOTE_COPY_DIR}

# Sets up the service
${REMOTE_CMD} sudo service cat_feeder stop
${REMOTE_CMD} "cd ${REMOTE_COPY_DIR}; sudo python ${REMOTE_COPY_DIR}setup.py install"
${REMOTE_CMD} sudo systemctl daemon-reload
${REMOTE_CMD} sudo service cat_feeder start
${REMOTE_CMD} sudo service cat_feeder status
