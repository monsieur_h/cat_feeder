import random
import smtplib
from email.mime.image import MIMEImage
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText


class MailSender(object):
    ADDRESSES = ["xx@gmail.com", "xxxxxxxxx@live.fr"]
    MESSAGES = [
        "Les chats sont nourris !",
        "Miam miam",
        "C'est bon, les croquettes sont dans le bol",
        "Ici petit robot, j'ai nourri Wallou et Foutou",
        "Les fauves sont repus",
        "Les chats mangents",
        "Des nouvelles des monstres",
        "Foutouna et Wallou...",
        "Wallousaure et sa maman mangent",
        "Bip bop, je nourris les chats",
        "Vrrmmmmm distribution de croquettes"
    ]

    def send(self, filename):
        msg = self._create_message()
        img = self._load_image(filename)
        msg.attach(img)
        self._smtp_send(msg)

    def _smtp_send(self, msg):
        smtp = smtplib.SMTP("smtp.bbox.fr")
        smtp.sendmail(msg['From'], msg['To'], msg.as_string())
        smtp.quit()

    def _load_image(self, filename):
        with open(filename, "rb") as file_stream:
            img = MIMEImage(file_stream.read())

        return img

    def _create_message(self):
        msg = MIMEMultipart()
        msg['Subject'] = self._get_random_content()
        msg['To'] = ", ".join(MailSender.ADDRESSES)
        msg['From'] = "wallou_&_foutouna@nidamour.fr"
        msg.preamble = self._get_random_content()
        msg.attach(MIMEText(self._get_random_content()))
        return msg

    def _get_random_content(self):
        return random.choice(MailSender.MESSAGES)
