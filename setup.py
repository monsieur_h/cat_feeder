import glob
import os
from distutils.core import setup

setup(
    name='cat_feeder',
    version='1.1',
    description="A daemon to feed my cats",
    author='Hubert Ray',
    author_email='ray.hubert@gmail.com',
    data_files=[
        ('/etc/systemd/system/', ['res/cat_feeder.service']),
        ('/usr/local/lib/cat_feeder/captures/', ['res/capture_folder']),
        ('/var/log/', ['res/cat_feeder.log']),
        ('/usr/local/lib/cat_feeder/', [file_name for file_name in os.listdir(".") if file_name.endswith("py")]),
        ('/usr/local/lib/cat_feeder/driver/', glob.glob(os.path.join('driver', '*.py'))),
        ('/usr/local/lib/cat_feeder/utils/', glob.glob(os.path.join('utils', '*.py'))),
    ]
)
