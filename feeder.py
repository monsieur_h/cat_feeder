#!/usr/bin/env python

import logging
import sys
import traceback
from Queue import Queue, Empty

import threaded_waiter
from constants import MessageConstants
from driver.button import PressButton
from driver.camera import USBCamera
from driver.led import Led
from driver.servomotor import Motor
from utils.mail_sender import MailSender

DEBUG = True


class CatFeederApp(object):
    MOTOR_PIN = 18
    BUTTON_PIN = 4  # TODO: Actual pin
    FEED_LED_PIN = 23
    FEED_ACTION_COUNT = 3  # TODO: Test
    REVS_PER_MINUTE = 15
    CAPTURE_DIR = "/usr/local/lib/cat_feeder/captures/"

    def __init__(self):
        # Member variables
        self.actions = {}
        self._running = True
        logging.info("Booting catfeeder")
        self.cat_fed = False
        self.queue = Queue()
        threaded_waiter.event_queue = self.queue
        threaded_waiter.main_thread_callback = self.socket_callback
        threaded_waiter.start_threaded_waiter()

        # Init process
        self.map_constants_to_actions()
        self.setup_GPIO()

        # Hardware devices
        self.motor = Motor(CatFeederApp.MOTOR_PIN)
        self.led = Led(CatFeederApp.FEED_LED_PIN)
        self.button = PressButton(CatFeederApp.BUTTON_PIN, self.button_callback)
        self.camera = USBCamera()

        # Software actions
        self.mailer = MailSender()

    @staticmethod
    def setup_GPIO():
        try:
            import RPi.GPIO as GPIO
        except RuntimeError:
            print("Error importing RPi.GPIO. Library not present or user is not root.")
            exit(-1)
        GPIO.setmode(GPIO.BCM)

    def map_constants_to_actions(self):
        self.actions[MessageConstants.FAKE_FEED] = self.fake_feed
        self.actions[MessageConstants.FEED] = self.feed
        self.actions[MessageConstants.FORCE_FEED] = self.force_feed
        self.actions[MessageConstants.FORCE_PHOTO] = self.force_photo
        self.actions[MessageConstants.RESET_FEED_VALUE] = self.reset_feed
        self.actions[MessageConstants.SIGKILL] = self.kill

    def poll(self):
        try:
            queue_item = self.queue.get()
            queue_item[0](queue_item[1])
        except Empty:
            pass

    def force_feed(self):
        logging.info("Starting force feed...")
        self._feed()

    def fake_feed(self):
        self.take_photo()

    def take_photo(self, feed=False):
        photo_path = self.camera.capture_to_file(self.CAPTURE_DIR, feed)
        self.mailer.send(photo_path)

    def run(self):
        try:
            while self._running:
                self.poll()
        except KeyboardInterrupt:
            logging.debug("Exiting on interrupt...")
        except Exception as e:
            logging.debug("Exit on unhandled exception :\n%s" % e.message)
            if DEBUG:
                traceback.print_exc(file=sys.stdout)
        finally:
            self.dispose()
            logging.info("Resource freed !")
            exit(0)

    def feed(self):
        if not self.cat_fed:
            self._feed()
        else:
            logging.warning("Cats were already fed, cancelling feeding...")

    def kill(self):
        self._running = False

    def _fill_bowl(self):
        for i in range(CatFeederApp.FEED_ACTION_COUNT):
            logging.debug("Moving motor %d / %d" % (i + 1, CatFeederApp.FEED_ACTION_COUNT))
            self._action_motor()

    def _action_motor(self, count=FEED_ACTION_COUNT):
            self._motor_movement()

    def _motor_movement(self):
        self.motor.run()

    def button_callback(self, channel):
        logging.debug("Received a button press ! (channel %s)", channel)
        self.force_feed()

    def socket_callback(self, constant):
        """
        :type constant: MessageConstants
        """
        constant = int(constant)
        logging.debug("Received value %s from network" % constant)
        if constant in self.actions:
            logging.debug("Executing function %s()" % self.actions[constant].func_name)
            self.actions[constant]()
        else:
            logging.error("Constant %d not found !" % constant)

    def _feed(self):
        self._fill_bowl()
        self.cat_fed = True
        self._update_led()
        self.take_photo(feed=True)

    def dispose(self):
        logging.info("Freeing app resources...")
        self.motor.dispose()
        self.button.dispose()
        self.led.dispose()
        threaded_waiter.server.shutdown()

    def force_photo(self):
        self.take_photo()

    def reset_feed(self):
        self.cat_fed = False
        self._update_led()

    def _update_led(self):
        self.led.status = self.cat_fed

    def __del__(self):
        self.dispose()


if __name__ == "__main__":
    logging.basicConfig(filename="/var/log/cat_feeder.log", level=logging.DEBUG,
                        format="%(asctime)-15s:%(levelname)s:%(message)s", datefmt="%Y-%m-%d %H:%M:%S")
    app = CatFeederApp()
    app.run()
