#! /usr/bin/env python

import argparse
import socket

from constants import MessageConstants, NetworkConstants


def send_msg_to_server(message_name):
    # noinspection PyCallByClass
    msg_num_code = MessageConstants.__getattribute__(MessageConstants, message_name)

    con = socket.create_connection((NetworkConstants.HOST, NetworkConstants.PORT))
    con.send(str(msg_num_code))
    con.close()


if __name__ == "__main__":
    network_consts = [constant for constant in dir(MessageConstants) if "__" not in constant]

    parser = argparse.ArgumentParser()
    parser.add_argument(dest="msg", help="The message constant to be sent", choices=network_consts)
    config = parser.parse_args()

    send_msg_to_server(config.msg)
