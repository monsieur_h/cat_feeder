import SocketServer
import threading
from Queue import Queue

from constants import NetworkConstants

event_queue = Queue()
main_thread_callback = None
server = None


class CallbackTCPServer(SocketServer.TCPServer):
    def __init__(self, server_address, RequestHandlerClass, callback, bind_and_activate=True):
        SocketServer.TCPServer.__init__(self, server_address, RequestHandlerClass, bind_and_activate)
        self.callback = callback

    def serve_forever(self, poll_interval=0.5):
        SocketServer.BaseServer.serve_forever(self, poll_interval)


class CallbackTCPRequestHandler(SocketServer.StreamRequestHandler):
    def handle(self):
        data = self.request.recv(1024)
        # with open("/tmp/debug.log", "w+") as logfile:
        #     logfile.write(str(self.server.callback))
        #     logfile.write(repr(self.server.callback))
        #     logfile.write(dir(self.server.callback))
        self.server.callback(data)


def callback_main_thread(data):
    event_queue.put((main_thread_callback, data))


def start_thread(callback_function):
    global server
    server = CallbackTCPServer((NetworkConstants.HOST, NetworkConstants.PORT), CallbackTCPRequestHandler,
                               callback_main_thread)
    server.serve_forever()


def socket_callback_mainthread(data):
    print("Received " + data)


def start_threaded_waiter():
    threading.Thread(target=start_thread, args=(main_thread_callback,)).start()


if __name__ == "__main__":
    try:
        waiter_thread = threading.Thread(target=start_thread, args=(socket_callback_mainthread,)).start()

        received_data = event_queue.get(True)
    except KeyboardInterrupt:
        pass
