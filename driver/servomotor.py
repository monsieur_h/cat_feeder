import time

import RPi.GPIO as GPIO

from driver import Driver


class Motor(Driver):
    def __init__(self, pin):
        self.pin = pin
        super(Motor, self).__init__()
        GPIO.setup(pin, GPIO.OUT)
        self.pwn = None

    def run(self):
        self.pwn = GPIO.PWM(self.pin, 50)
        self.pwn.stop()
        self.pwn.start(0)
        for dc in range(0, 10, 5):
            self.pwn.ChangeDutyCycle(dc)
            time.sleep(0.75)
        for dc in range(11, -1, -5):
            self.pwn.ChangeDutyCycle(dc)
            time.sleep(0.75)

        self.pwn.ChangeDutyCycle(0)
        self.pwn.stop()

    def dispose(self):
        super(Motor, self).dispose()
        GPIO.cleanup(self.pin)
