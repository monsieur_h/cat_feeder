import datetime
import os
import os.path
import shutil
import unittest

import camera


class TestImageCapture(unittest.TestCase):
    def setUp(self):
        super(TestImageCapture, self).setUp()
        self.cam = camera.USBCamera()
        self.test_folder = "/tmp/testImageCapture/"
        self.given_folder(self.test_folder)

    def test_camera_capture(self):
        created_filename = self.cam.capture_to_file(self.test_folder)
        self.assertTrue(os.path.exists(created_filename), "File not found : {}".format(created_filename))

    def test_file_read(self):
        timestamp = datetime.datetime.now()
        filename = self.cam._generate_filename(feed=True, timestamp=timestamp)
        file_path = os.path.abspath(self.test_folder + filename)
        self.given_file(file_path)

        snapshot = camera.ImageSnapshot(file_path)
        self.assertTrue(snapshot.feed)
        self.verify_datetimes(timestamp, snapshot.timestamp)

    def tearDown(self):
        super(TestImageCapture, self).tearDown()
        if os.path.exists(self.test_folder):
            pass
            shutil.rmtree(self.test_folder)

    def given_folder(self, folder_name):
        if not os.path.exists(folder_name):
            os.mkdir(folder_name)

    def given_file(self, file_path):
        with open(file_path, "w+") as file_stream:
            file_stream.write("0")

    def verify_datetimes(self, timestamp, timestamp1):
        self.assertEqual(timestamp.day, timestamp1.day)
        self.assertEqual(timestamp.month, timestamp1.month)
        self.assertEqual(timestamp.year, timestamp1.year)
        self.assertEqual(timestamp.hour, timestamp1.hour)
        self.assertEqual(timestamp.minute, timestamp1.minute)


if __name__ == '__main__':
    unittest.main()
