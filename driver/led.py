import RPi.GPIO as GPIO

from driver import Driver


class Led(Driver):
    def __init__(self, pin):
        self.pin = pin
        self._status = False
        GPIO.setup(self.pin, GPIO.OUT, initial=False)

    @property
    def status(self):
        return self._status

    @status.setter
    def status(self, new_status):
        GPIO.output(self.pin, new_status)
        self._status = new_status

    def dispose(self):
        GPIO.cleanup(self.pin)
