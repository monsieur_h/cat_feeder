import RPi.GPIO as GPIO


class PressButton(object):
    DEFAULT_BOUNCE_THRESHOLD = 200

    def __init__(self, pin, callback):
        self.pin = pin
        GPIO.setup(self.pin, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
        GPIO.add_event_detect(pin, GPIO.RISING, callback=callback, bouncetime=PressButton.DEFAULT_BOUNCE_THRESHOLD)

    def dispose(self):
        GPIO.cleanup(self.pin)
