import datetime
import os.path
import subprocess

DATE_FORMAT = "%Y-%m-%d@%H:%M"


class USBCamera(object):
    def capture_to_file(self, path, feed=False):
        file_name = self._generate_filename(feed)
        abspath = os.path.abspath(path + file_name + ".jpeg")
        subprocess.check_call([
            "fswebcam", abspath, "-r", "800x600", "-p", "YUYV"])
        return abspath

    @staticmethod
    def _generate_filename(feed=False, timestamp=None):
        if not feed:
            feed_name = "manualshot"
        else:
            feed_name = "feedshot"

        if not timestamp:
            timestamp = datetime.datetime.now()
        file_name = feed_name + datetime.datetime.strftime(timestamp, DATE_FORMAT)
        return file_name


class ImageSnapshot(object):
    def __init__(self, image_path):
        """

        :type image_path: str
        """
        super(ImageSnapshot, self).__init__()
        self.feed = False
        self.path = image_path
        image_path = os.path.basename(image_path)

        if "feedshot" in image_path:
            self.feed = True
        image_path = image_path.replace("feedshot", "").replace("manualshot", "").replace(".jpeg", "")
        file_date = datetime.datetime.strptime(image_path, DATE_FORMAT)
        self.timestamp = file_date
