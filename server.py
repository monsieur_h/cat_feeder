import SocketServer
import socket
import threading
import time


class ThreadedTCPRequestHandler(SocketServer.BaseRequestHandler):
    def handle(self):
        data = self.request.recv(1024)
        cur_thread = threading.current_thread()
        response = "{}: {}".format(cur_thread.name, data)
        self.request.sendall(response)


class ThreadedTCPRequestCallbackHandler(ThreadedTCPRequestHandler):
    def __init__(self, request, client_address, server, callback):
        SocketServer.BaseRequestHandler.__init__(self, request, client_address, server)
        self._callback = callback

    def handle(self):
        data = self.request.recv(1024)
        self._callback(data)


class ThreadedTCPServer(SocketServer.ThreadingMixIn, SocketServer.TCPServer):
    pass


if __name__ == "__main__":

    def client(ip, port, message):
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.connect((ip, port))
        try:
            sock.sendall(message)
            response = sock.recv(1024)
            print "Received: {}".format(response)
        finally:
            sock.close()


    HOST, PORT = "localhost", 9999

    server = ThreadedTCPServer((HOST, PORT), ThreadedTCPRequestHandler)
    ip, port = server.server_address

    # Start a thread with the server -- that thread will then start one
    # more thread for each request
    server_thread = threading.Thread(target=server.serve_forever)
    # Exit the server thread when the main thread terminates
    server_thread.daemon = True
    server_thread.start()
    print "Server loop running in thread:", server_thread.name

    client(ip, port, "Hello World 1")
    client(ip, port, "Hello World 2")
    client(ip, port, "Hello World 3")
    try:
        print("Waiting for connections on %s:%s", HOST, PORT)
        time.sleep(3600)
    except KeyboardInterrupt:
        server.shutdown()
        server.server_close()
