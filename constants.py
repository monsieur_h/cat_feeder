class MessageConstants(object):
    FEED = 1
    FORCE_FEED = 2
    FAKE_FEED = 4
    FORCE_PHOTO = 8
    RESET_FEED_VALUE = 16
    SIGKILL = 32


class NetworkConstants(object):
    HOST = "localhost"
    PORT = 9999
